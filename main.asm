%include "colon.inc"

section .bss
buff: resb 256

section .rodata
out_of_buffer_msg: db "Length of string is longer than buffer", 0
fail_finding_msg: db "There is no such string in the dictionary as this", 0
%include "words.inc"

section .data


section .text
%include "dict.inc"
%include "lib.inc"

global _start



_start:
	
	xor rcx, rcx
	
	.loop:
		
		push rcx
		call read_char
		pop rcx
		
		
		test al, al
		je .find_str
		cmp al, 0xA
		je .find_str
		
		cmp rcx, 255
		je .out_of_buffer
		
		mov byte[buff + rcx], al
		inc rcx
		
		jmp .loop
	
	.find_str:
		mov rdi, buff
		mov rsi, HEAD
		call find_word
		test rax, rax
		je .fail_finding
		

	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	
	add rdi, rax
	inc rdi
	
	
	.ok:
		call print_string
		jmp .end
		
	.out_of_buffer:
		mov rdi, out_of_buffer_msg
		jmp .err
		
	.fail_finding:
		mov rdi, fail_finding_msg
		
	.err:
		call print_err
		mov rdi, 1
		call exit
		
		
	.end:
		call print_newline
		xor rdi, rdi
		call exit