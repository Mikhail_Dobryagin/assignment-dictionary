%include "lib.inc"


section .text

; rdi -- Указатель на нуль-терминированную строку.
; rsi -- Указатель на начало словаря.

; find_word пройдёт по всему словарю в поисках подходящего ключа.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
global find_word
find_word:

	.loop:
		test rsi, rsi
		je .fail
		add rsi, 8
		
		push rsi
		push rdi
		
		call string_equals
		
		pop rdi
		pop rsi
		
		sub rsi, 8
		
		test rax, rax
		jne .end
		mov rsi, [rsi]
		jmp .loop
	
	.end:
		mov rax, rsi
		ret
	
	.fail:
		xor rax, rax
		ret