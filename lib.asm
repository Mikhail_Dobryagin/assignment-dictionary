section .text

 

global exit 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину

global string_length

string_length:

	xor rax, rax
    .loop:
		cmp byte[rdi + rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout

global print_string

print_string:

    push rdi
	call string_length
	pop rsi
	mov rdx, rax
	xor rax, rax
	inc rax
	xor rdi, rdi
	inc rdi
	syscall

	ret

	
global print_err
print_err:

    push rdi
	call string_length
	pop rsi
	mov rdx, rax
	xor rax, rax
	inc rax
	mov rdi, 2
	syscall

	ret

	


; Принимает код символа и выводит его в stdout

global print_char

print_char:

    push rdi

	xor rax, rax

	inc rax

	xor rdi, rdi

	inc rdi

	mov rsi, rsp

	xor rdx, rdx

	inc rdx

	syscall

	pop rdi

    ret



; Переводит строку (выводит символ с кодом 0xA)

global print_newline

print_newline:

    push 10

	xor rax, rax

	inc rax

	xor rdi, rdi

	inc rdi

	mov rsi, rsp

	xor rdx, rdx

	inc rdx

	syscall

	pop r10

    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 

; Совет: выделите место в стеке и храните там результаты деления

; Не забудьте перевести цифры в их ASCII коды.

global print_uint

print_uint:

	push 0

    mov r10, 10

	mov rax, rdi

	

	.loop:

		xor rdx, rdx

		div r10

		add rdx, '0'

		push rdx

		cmp rax, 0

		je .print

		jmp .loop

		

	.print:

		pop rdi

		cmp rdi, 0

		je .end

		call print_char

		jmp .print

		

	.end:

		ret



; Выводит знаковое 8-байтовое число в десятичном формате 

global print_int

print_int:

    cmp rdi, 0

	jge .print

	push rdi

	mov rdi, '-'

	call print_char

	pop rdi

	neg rdi

	

	.print:

		call print_uint

    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

global string_equals

string_equals:

    xor rax, rax

	.loop:

		mov al, byte[rdi]

		cmp al, byte[rsi]

		jne .false

		inc rdi

		inc rsi

		

		cmp al, 0

		jne .loop

		

	.true:

		xor rax, rax

		inc rax

		ret

	

	.false:

		xor rax,rax

		ret

	



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока

global read_char

read_char:

	

    xor rax, rax

	xor rdi, rdi

	push rax

	mov rsi, rsp

	xor rdx, rdx

	inc rdx

	syscall

	

	pop rax

    ret 



; Принимает: адрес начала буфера, размер буфера

; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .

; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.

; Останавливается и возвращает 0 если слово слишком большое для буфера

; При успехе возвращает адрес буфера в rax, длину слова в rdx.

; При неудаче возвращает 0 в rax

; Эта функция должна дописывать к слову нуль-терминатор

global read_word

read_word:

	

	dec rsi

	

	mov r10, rdi ;начало буфера

	xor r11, r11 ;длина

	push r12

	mov r12, rsi ;размер буфера

	

	

	

	.skip_spaces:

		push r10

		push r11

		call read_char

		pop r11

		pop r10

		

		cmp al, 0

		je .end

		cmp al, 0x20

		je .skip_spaces

		cmp al, 0x9

		je .skip_spaces

		cmp al, 0xA

		je .skip_spaces

		

	.read:

		cmp al, 0

		je .end

		cmp al, 0x20

		je .end

		cmp al, 0x9

		je .end

		cmp al, 0xA

		je .end

		

		cmp r11, r12

		je .fail

		

		mov byte[r10+r11], al

		inc r11

		

		

		push r10

		push r11

		call read_char

		pop r11

		pop r10

		

		jmp .read

		

		

		

	.fail:

		xor rax, rax

		xor rdx, rdx

		pop r12

		ret

 

	.end:

		mov rax, r10

		mov rdx, r11

		mov byte [r10 + r11], 0

		pop r12

		ret

		

		

; Принимает указатель на строку, пытается

; прочитать из её начала беззнаковое число.

; Возвращает в rax: число, rdx : его длину в символах

; rdx = 0 если число прочитать не удалось

global parse_uint

parse_uint:

	

	xor rax, rax

	mov r10, 10

	xor r11, r11 ;длина

	push r12

	xor r12, r12 ;прочитанная цифра

	

	mov al, byte[rdi]

	cmp al, '0'

	jb .fail

	cmp rax, '9'

	ja .fail

	

	sub al, '0'

	

	inc rdi

	inc r11

	

	.loop:

		mov r12b, byte[rdi]

		cmp r12b, '0'

		jb .end

		cmp r12b, '9'

		ja .end

		sub r12b, '0'

		mul r10

		add rax, r12

		inc rdi

		inc r11

		jmp .loop

	

	.fail:

		xor rax, rax

		xor rdx, rdx

		pop r12

		ret

	.end:

		mov rdx, r11

		pop r12

		ret









; Принимает указатель на строку, пытается

; прочитать из её начала знаковое число.

; Если есть знак, пробелы между ним и числом не разрешены.

; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 

; rdx = 0 если число прочитать не удалось

global parse_int

parse_int:

    xor rax, rax

	

	mov al, byte[rdi]

	

	cmp al, '-'

	

	jne .pos

	

	.neg:

		inc rdi

		call parse_uint

		neg rax

		cmp rdx, 0

		je .fail

		inc rdx

		ret

	.pos:

		call parse_uint

		ret

		

	.fail:

		xor rax, rax

		xor rdx, rdx

		ret



; Принимает указатель на строку, указатель на буфер и длину буфера

; Копирует строку в буфер

; Возвращает длину строки если она умещается в буфер, иначе 0

global string_copy

string_copy:

	

	push rdx

	xor rax, rax

	

    .loop:

		cmp rdx, 0

		je .fail

		

		mov al, byte[rdi]

		mov byte[rsi], al

		inc rsi

		inc rdi

		dec rdx

		

		cmp al, 0

		je .end

		

		jmp .loop

	

	.fail:

		pop rax

		xor rax, rax

		ret

		

	.end:

		pop rax

		sub rax, rdx

		ret