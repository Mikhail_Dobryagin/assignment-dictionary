%macro colon 2
	%2:
	%ifdef HEAD
		dq HEAD
	%else
		dq 0
	%endif
	
	%define HEAD %2
	db %1, 0
%endmacro
